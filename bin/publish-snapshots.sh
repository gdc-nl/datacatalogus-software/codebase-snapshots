#!/bin/sh

set -eu

PROJECT_ROOT=$(realpath "$(dirname "$0")/..")
cd "${PROJECT_ROOT}" || exit 1

if [ "$(git status --porcelain | wc -l)" -gt "0" ]; then
    echo "Repository has local changes, refusing to proceed" && exit 1
fi

./bin/make-snapshots.sh

if [ "$(git status --porcelain | wc -l)" -gt "0" ]; then
    git add ./snapshots/*.zip
    git commit --message "[skip ci] Snapshots $(date -d 'now' '+%Y-%m-%d')"
    git push origin -o ci.skip
else
    echo "Snapshots are up-to-date"
fi
