#!/bin/sh

set -eu

PROJECT_ROOT=$(realpath "$(dirname "$0")/..")
cd "${PROJECT_ROOT}" || exit 1

REPOSITORIES=""
REPOSITORIES="${REPOSITORIES} xpertselect/portals/ckanext-portals"
REPOSITORIES="${REPOSITORIES} xpertselect/portals/dcat-suite"
REPOSITORIES="${REPOSITORIES} xpertselect/portals/drupal-suite"
REPOSITORIES="${REPOSITORIES} xpertselect/portals/solr-configsets"
REPOSITORIES="${REPOSITORIES} xpertselect/portals/woo-suite"
REPOSITORIES="${REPOSITORIES} xpertselect/portals/xpertselect-theme"
REPOSITORIES="${REPOSITORIES} xpertselect/ckan-sdk"
REPOSITORIES="${REPOSITORIES} xpertselect/json-api"
REPOSITORIES="${REPOSITORIES} xpertselect/portals-sdk"
REPOSITORIES="${REPOSITORIES} xpertselect/psr-tools"
REPOSITORIES="${REPOSITORIES} xpertselect/psr-traits"
REPOSITORIES="${REPOSITORIES} xpertselect/tools"

for REPOSITORY in ${REPOSITORIES}; do
    echo "Repository: ${REPOSITORY}"

    # The '/' characters must be URL-encoded
    # Ref: https://docs.gitlab.com/ee/api/rest/index.html#namespaced-path-encoding
    ENCODED_REPOSITORY=$(echo "${REPOSITORY}" | sed -e 's#/#'%2f'#g')

    # Generate the snapshot filename
    # Input: path/to/repository-name
    # Output: path_to_repository-name
    TARGET_REPOSITORY=$(echo "${REPOSITORY}" | sed -e 's#/#'_'#g')
    TARGET_FILE=$(realpath "./snapshots/${TARGET_REPOSITORY}.zip")

    rm -f "${TARGET_FILE}"
    curl "https://gitlab.com/api/v4/projects/${ENCODED_REPOSITORY}/repository/archive.zip" \
        --output "${TARGET_FILE}" \
        --location \
        --silent

    # Prevent Gitlab.com API rate-limit.
    sleep 15
done
