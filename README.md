# GDC Samenwerking Nederland / Datacatalogus software / Codebase snapshots

[gitlab.com/gdc-nl/datacatalogus-software/codebase-snapshots](https://gitlab.com/gdc-nl/datacatalogus-software/codebase-snapshots.git)

Deze repository bevat snapshots van de open source [XpertSelect Portals](https://gitlab.com/xpertselect/portals) 
codebase die ingezet wordt als de GDC-datacatalogussoftware. Deze snapshots garanderen dat de gebruikte code 
publiekelijk beschikbaar is en blijft.

Van de onderstaande [Gitlab.com](https://gitlab.com/) projecten worden snapshots gemaakt:

- [gitlab.com/xpertselect/portals/ckanext-portals](https://gitlab.com/xpertselect/portals/ckanext-portals)
- [gitlab.com/xpertselect/portals/dcat-suite](https://gitlab.com/xpertselect/portals/dcat-suite)
- [gitlab.com/xpertselect/portals/drupal-suite](https://gitlab.com/xpertselect/portals/drupal-suite)
- [gitlab.com/xpertselect/portals/solr-configsets](https://gitlab.com/xpertselect/portals/solr-configsets)
- [gitlab.com/xpertselect/portals/woo-suite](https://gitlab.com/xpertselect/portals/woo-suite)
- [gitlab.com/xpertselect/portals/xpertselect-theme](https://gitlab.com/xpertselect/portals/xpertselect-theme)
- [gitlab.com/xpertselect/ckan-sdk](https://gitlab.com/xpertselect/ckan-sdk)
- [gitlab.com/xpertselect/json-api](https://gitlab.com/xpertselect/json-api)
- [gitlab.com/xpertselect/portals-sdk](https://gitlab.com/xpertselect/portals-sdk)
- [gitlab.com/xpertselect/psr-tools](https://gitlab.com/xpertselect/psr-tools)
- [gitlab.com/xpertselect/psr-traits](https://gitlab.com/xpertselect/psr-traits)
- [gitlab.com/xpertselect/tools](https://gitlab.com/xpertselect/tools)

## Gebruik

Met onderstaande instructies worden nieuwe snapshots gemaakt:

```shell
git clone https://gitlab.com/gdc-nl/datacatalogus-software/codebase-snapshots.git
cd codebase-snapshots
./bin/make-snapshots.sh
```

Nieuwe versies van de snapshots worden in de `./snapshots` map geplaatst. Het `make-snapshots.sh` script respecteert de
[Gitlab API rate-limit](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlabcom-specific-rate-limits) door niet
meer dan 4 snapshots per minuut te maken.

## Git LFS

Deze repository maakt gebruik van [Git LFS](https://git-lfs.github.com/) om de codebase snapshots te beheren.
